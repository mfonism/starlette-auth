import pathlib

import databases
from starlette.config import Config
from starlette.datastructures import Secret

BASE_DIR = pathlib.Path(__file__).parent.parent.absolute()
config = Config(BASE_DIR / ".env")

# Mode flags
DEBUG = config("DEBUG", cast=bool, default=False)
TESTING = config("TESTING", cast=bool, default=False)

# Secret key
SECRET_KEY = config("SECRET_KEY", cast=Secret)

# Other configs
DATABASE_URL = config("DATABASE_URL", cast=databases.DatabaseURL)
if TESTING:
    DATABASE_URL = DATABASE_URL.replace(database="test_" + DATABASE_URL.database)

if DATABASE_URL.dialect == "postgres":
    DATABASE_URL = DATABASE_URL.replace(dialect="postgresql")  # pragma: nocover
