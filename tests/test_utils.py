import pytest

from starlauth import utils
from starlauth.exceptions import ExpiredToken, InvalidToken


@pytest.mark.parametrize(
    "plaintext, plaintext_with_error",
    [
        ("I said HELLO, world!", "I said hello, world!"),
        (
            "This is not the way the world was meant to end",
            "This is not the way the word was meant to end",
        ),
        ("I'm on my 3rd passphrase, really!", "I'm on my third passphrase, really!"),
    ],
)
def test_passphrase_hashing(plaintext, plaintext_with_error):
    hashed = utils.get_passphrase_hash(plaintext)
    assert plaintext != hashed
    assert utils.check_passphrase(plaintext, hashed)

    assert plaintext != plaintext_with_error
    assert not utils.check_passphrase(plaintext_with_error, hashed)


@pytest.mark.parametrize(
    "email", ["hello@world.com", "here@i.come", "here@i.go", "good@bye.world"]
)
def test_email_confirmation_token(mock_get_timestamp, email):
    init_age = 1000
    mock_get_timestamp.return_value = init_age
    token = utils.get_email_confirmation_token(email)
    assert token != email
    assert utils.decode_email_confirmation_token(token) == email

    # decoding raises `starlauth.exceptions.ExpiredToken` if
    # `max_age` is supplied and token is older than that
    max_age = 48 * 60 * 60
    mock_get_timestamp.return_value += max_age + 1
    with pytest.raises(ExpiredToken):
        assert utils.decode_email_confirmation_token(token, max_age=max_age)

    mock_get_timestamp.return_value = init_age
    invalid_token = token[:-1]
    with pytest.raises(InvalidToken):
        assert utils.decode_email_confirmation_token(invalid_token)
